---
title : "IT hygiene validator"
description: "Documentation for the IT hygiene validator"
lead: "Validator that checks several standards that are required by the Dutch government."
date: 2023-06-01T14:52:40+02:00
draft: false
toc: true
---

*You can find the repository on [GitLab](https://gitlab.com/digilab.overheid.nl/ecosystem/it-hygiene-validator).*

## Online version

This validator is available online at [it-hygiene-validator.core.digilab.network](https://it-hygiene-validator.core.digilab.network/).


## Checks

The validator checks several standards that are listed on the [Dutch government website](https://forumstandaardisatie.nl/open-standaarden/verplicht). The following checks are implemented:
- SPF
- DMARC
- DKIM
- TLS
- DNSSEC
- Security headers


## Running locally

Clone [this repo](https://gitlab.com/digilab.overheid.nl/ecosystem/it-hygiene-validator). Note: static files (css/scss/js/img) are stored in the `static` dir, but built into the `public` dir.

When running for the first time, make sure `pnmp` is installed (e.g. using `brew install pnpm`) and run `pnpm install`.

Run:

```sh
pnpm run dev
go run main.go -port 8080
```

This starts a web server locally on the specified port.
