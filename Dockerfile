# Stage 1
FROM golang:1.20-alpine3.18 AS builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build
COPY . .

# Build the Go files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .


# Stage 2
FROM node:18-alpine3.18 AS node_builder

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY . .

# Build the static files
RUN corepack enable && corepack prepare pnpm@latest --activate
RUN pnpm install
RUN pnpm run build


# Stage 3
FROM alpine:3.18

# Install common CA certificates, required for TLS checks
RUN apk update && apk add ca-certificates

# Copy the binary from /build to the root folder of this container. Also copy the public dir that was built
COPY --from=builder /build/server /
COPY --from=node_builder /build/public /public

# Copy the views dir and resolv.conf, which are read during runtime
COPY views views
COPY resolv.conf resolv.conf

# Run the binary when starting the container
ENTRYPOINT ["/server"]

EXPOSE 80
