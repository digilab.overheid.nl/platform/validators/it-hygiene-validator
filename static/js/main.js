// Imports
import 'htmx.org';


// Explanation links: when clicked, open/expand the linked details and smoothly scroll down
function openTarget() {
  const hash = location.hash.substring(1);
  if (hash) {
    var details = document.getElementById(hash);
    if (details && details.tagName.toLowerCase() === 'details') {
      details.open = true;

      // Scroll the element into view
      details.scrollIntoView({ behavior: 'smooth' });
    }
  }
}

window.addEventListener('hashchange', openTarget);
openTarget(); // For the initial hash
