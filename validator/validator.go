package validator

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/miekg/dns"
	"github.com/peterzen/goresolver"
	"golang.org/x/sync/errgroup"
)

type result struct {
	Valid   bool
	Details string
}

var spfRegExp = regexp.MustCompile(`^\s*v=spf1`)
var dmarcRegExp = regexp.MustCompile(`^\s*v=DMARC\d+`)
var dkimRegExp = regexp.MustCompile(`^\s*v=DKIM\d+`)

func Validate(inputURL string) (results map[string]result) {
	results = make(map[string]result)

	// Get all subdomains of the input URL
	parsedURL, err := url.Parse(inputURL)
	if err != nil {
		mess := fmt.Sprintf("the specified url seems to be invalid: %v", err)

		results["SPF"] = result{Details: mess}
		results["DMARC"] = result{Details: mess}
		results["DKIM"] = result{Details: mess}
		results["TLS"] = result{Details: mess}
		results["DNSSEC"] = result{Details: mess}

		return
	}

	// Get all parent domains for the hostname, including the hostname itself
	hostname := parsedURL.Hostname()
	domainParts := strings.Split(hostname, ".")
	subdomains := make([]string, len(domainParts)-1)

	for i := 0; i < len(domainParts)-1; i++ {
		subdomains[i] = strings.Join(domainParts[i:], ".")
	}

	// Loop over all subdomains, since SPF and DMARC might be set on any subdomain
	for _, subdomain := range subdomains {
		// IMPROVE: check the following concurrently

		// If no valid SPF record found yet, check for an SPF record on the current subdomain
		if !results["SPF"].Valid {
			results["SPF"] = checkSPF(subdomain)
		}

		// If no valid DMARC record found yet, check for a DMARC record on the current subdomain. Note: although DMARC does not propagate to all subdomains, email may be sent from a parent domain. So when a DMARC record exists on a parent domain, we let the validator pass
		if !results["DMARC"].Valid {
			results["DMARC"] = checkDMARC(subdomain)
		}

		// If no valid DKIM record found yet, check for a DKIM record on the current subdomain
		if !results["DKIM"].Valid {
			results["DKIM"] = checkDKIM(subdomain)
		}
	}

	if res := results["DMARC"]; !res.Valid {
		res.Details = fmt.Sprintf("No DMARC record found for hostname %s", hostname)
		if len(subdomains) > 1 {
			res.Details += " and its parent domains"
		}
		results["DMARC"] = res
	}

	if res := results["DKIM"]; !res.Valid {
		res.Details = fmt.Sprintf("No DKIM record found for common DKIM selectors for hostname %s", hostname)
		if len(subdomains) > 1 {
			res.Details += " and its parent domains"
		}
		results["DKIM"] = res
	}

	// Validate hostname TLS. IMPROVE: give an error/warning if the specified URL has scheme other than https?
	results["TLS"] = checkTLS(hostname)

	// Validate DNSSEC
	results["DNSSEC"] = checkDNSSEC(hostname)

	// Validate security respsonse headers of the specified URL
	results["Security headers"] = checkSecurityHeaders(inputURL)

	return
}

func checkSPF(subdomain string) (res result) {
	// Get all TXT DNS records of the subdomain
	txtRecords, err := net.LookupTXT(subdomain)
	if err != nil {
		res.Details = fmt.Sprintf("Error getting TXT records for domain %s: %v", subdomain, err)
	}

	for _, rec := range txtRecords {
		// Check if the current record is an SPF record
		if spfRegExp.MatchString(rec) {
			res = result{
				Valid:   true,
				Details: fmt.Sprintf("SPF record found for domain %s: %s", subdomain, rec),
			}
			// IMPROVE: validate the record content

			return // Assume only one SPF record exists
		}
	}

	return
}

func checkDMARC(subdomain string) (res result) {
	dmarcRecords, err := net.LookupTXT("_dmarc." + subdomain)
	if err != nil {
		// Do not return an error, since the _dmarc subdomain might simply not exist
		return
	}

	for _, rec := range dmarcRecords {
		// Check if the current record is a DMARC record
		if dmarcRegExp.MatchString(rec) {
			res = result{
				Valid:   true,
				Details: fmt.Sprintf("DMARC record found for domain _dmarc.%s: %s", subdomain, rec),
			}
			// IMPROVE: validate the record content

			return // Assume only one DMARC record exists
		}
	}

	return
}

func checkDKIM(subdomain string) (res result) {
	// Check for a list of commonly used email providers. Note: DKIM is hard to verify without an email message, so this check does not include custom mail clients. See also https://github.com/vavkamil/dkimsc4n/blob/master/dkim.lst for a list
	selectors := []string{
		"google",                 // Default for Gmail / Google Workspace, see https://support.google.com/a/answer/180504
		"selector1", "selector2", // Outlook, see https://learn.microsoft.com/en-us/microsoft-365/security/office-365-security/email-authentication-dkim-configure
		"k1", "k2", "k3", "k4", "k5", // Mailchimp / Mandrill
		"m1", "m2", "m3", "m4", "m5", // SendGrid
		"s1", "s2", "s5", "s7", // SendGrid
		"dkim",                // Hetzner
		"cm",                  // Campaign Monitor
		"mandrill",            // Mandrill
		"zendesk", "zendesk1", // Zendesk
		"mail", "default", "pm", "smtp", "smtpapi", "test", // Misc
	}

	// Check the TXT records of the resulting hostnames concurrently
	g := new(errgroup.Group)
	var mu sync.Mutex

	for _, selector := range selectors {
		selector := selector // Shadow the variable to be able to use it concurrently

		g.Go(func() error {
			dkimHostname := fmt.Sprintf("%s._domainkey.%s", selector, subdomain)

			dkimRecords, err := net.LookupTXT(dkimHostname)
			if err != nil {
				// Do not return an error, since the checked DKIM hostname host might simply not exist
				return nil
			}

			for _, rec := range dkimRecords {
				// Check if the current record is a DKIM record
				if dkimRegExp.MatchString(rec) {
					mu.Lock()
					res = result{
						Valid:   true,
						Details: fmt.Sprintf("DKIM record found for domain %s: %s", subdomain, rec),
					}
					mu.Unlock()
					// IMPROVE: validate the record content

					return nil // Assume only one DKIM record exists
				}
			}

			return nil
		})
	}

	// Wait for all requests to complete. IMPROVE: return immediately when some DKIM record found?
	g.Wait() // Note: no errors are returned by the functions, so we do not handle the value of g.Wait()

	return
}

func checkTLS(hostname string) (res result) {
	conn, err := tls.Dial("tcp", hostname+":443", nil)
	if err != nil {
		res.Details = fmt.Sprintf("Error dailing hostname %s:443: %v", hostname, err)
		return
	}

	if err = conn.VerifyHostname(hostname); err != nil {
		res.Details = fmt.Sprintf("Could not verify hostname %s: %v", hostname, err)
		return
	}

	certs := conn.ConnectionState().PeerCertificates
	if len(certs) == 0 {
		res.Details = fmt.Sprintf("No certificates found for hostname %s", hostname)
		return
	}

	for _, cert := range certs {
		if now := time.Now(); now.After(cert.NotAfter) {
			res.Details = fmt.Sprintf("Certificate for hostname %s has expired at %s", hostname, cert.NotAfter.Format(time.RFC850))
			return
		} else if now.Before(cert.NotBefore) {
			res.Details = fmt.Sprintf("Certificate for hostname %s is only valid from %s", hostname, cert.NotBefore.Format(time.RFC850))
			return
		}
	}

	res.Valid = true

	return
}

func checkDNSSEC(hostname string) (res result) {
	// Check the SOA record of the URL's hostname, similar to what the test tool of internet.nl does, see https://internet.nl/test-site/
	// IMPROVE: check more details, similar to https://dnssec-debugger.verisignlabs.com/
	resolver, err := goresolver.NewResolver("resolv.conf")
	if err != nil {
		res.Details = fmt.Sprintf("Cannot create DNS resolver: %v", err)
		return
	}

	if _, err := resolver.StrictNSQuery(dns.Fqdn(hostname), dns.TypeSOA); err != nil {
		res.Details = fmt.Sprintf("DNSSEC for hostname %s seems invalid: %v", hostname, err)
	}

	// Else...
	res.Valid = true
	return
}

func checkSecurityHeaders(inputURL string) (res result) {
	// Send a GET HTTP request to the specified URL
	resp, err := http.Get(inputURL)
	if err != nil {
		res.Details = fmt.Sprintf("Could not verify the URL because of the following error: %v", err)
		return
	}
	defer resp.Body.Close()

	expectedHeaders := []string{
		// Note: assume we specify headers here only in canonical format, because that is how headers are stored in http.Response.Header, see https://pkg.go.dev/net/http#CanonicalHeaderKey
		"Strict-Transport-Security",
		"X-Frame-Options", // Note: both possible options (DENY and SAMEORIGIN) are regarded fine, see also the check below
		"X-Content-Type-Options",
		"Content-Security-Policy",
		"Referrer-Policy",
		// Note: the following are mentioned in https://www.cip-overheid.nl/media/clkmwp3x/20200720-ssd-normen-v30.pdf as recommended, but not checked as these are non-standard or deprecated. Both of them are not used on overheid.nl itself
		// "X-XSS-Protection", // Non-standard, see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
		// "Expect-CT", // Deprecated, see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT
	}

	var recommendedHeaders []string
	for _, header := range expectedHeaders {
		if _, ok := resp.Header[header]; !ok {
			recommendedHeaders = append(recommendedHeaders, header)
		}
	}

	var details []string
	if len(recommendedHeaders) != 0 {
		details = append(details, fmt.Sprintf("The following response header(s) are recommended to be set: %s", strings.Join(recommendedHeaders, ", ")))
	}

	// Perform extra checks on several headers
	if vals, ok := resp.Header["X-Frame-Options"]; ok {
		for _, val := range vals {
			if strings.Contains(strings.ToLower(val), "ALLOW-FROM") {
				details = append(details, "The ALLOW-FROM directive in X-Frame-Options is deprecated")
				break
			}
		}
	}

	if vals, ok := resp.Header["Access-Control-Allow-Origin"]; ok {
		for _, val := range vals {
			if strings.TrimSpace(val) == "*" {
				details = append(details, "The Access-Control-Allow-Origin header should not be '*'")
				break
			}
		}
	}

	if vals, ok := resp.Header["X-Content-Type-Options"]; ok {
		for _, val := range vals {
			if !strings.EqualFold(strings.TrimSpace(val), "nosniff") {
				details = append(details, "The X-Content-Type-Options header should be 'nosniff'")
				break
			}
		}
	}

	// Check the use of the Referrer-Policy header, which is recommended to be used e.g. on https://www.cip-overheid.nl/media/clkmwp3x/20200720-ssd-normen-v30.pdf
	if vals, ok := resp.Header["Referrer-Policy"]; ok {
		for _, val := range vals {
			switch strings.ToLower(strings.TrimSpace(val)) {
			case "strict-origin-when-cross-origin", "strict-origin", "no-referrer":
				// Nothing to do, these are regarded safe, see e.g. https://web.dev/referrer-best-practices/#why-strict-origin-when-cross-origin-or-stricter

			default:
				details = append(details, fmt.Sprintf("The Referrer-Policy header should preferably be set to 'strict-origin-when-cross-origin' instead of '%s'", val))
			}
		}
	}

	// IMPROVE: validate Strict-Transport-Security syntax, see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security#syntax

	if len(details) == 0 {
		res.Valid = true
		return
	}

	// Else...
	res.Details = strings.Join(details, ". ")

	return
}
