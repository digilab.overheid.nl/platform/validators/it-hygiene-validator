package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"

	"gitlab.com/digilab.overheid.nl/ecosystem/it-hygiene-validator/validator"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	engine := html.New("./views", ".html")

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", nil)
	})

	app.Get("/validate", func(c *fiber.Ctx) error {
		inputURL := c.FormValue("url")

		data := fiber.Map{
			"title":   fmt.Sprintf("Results for %s — IT hygiene validator — Digilab", inputURL), // IMPROVE: set this in the view somehow
			"url":     inputURL,
			"results": validator.Validate(inputURL),
		}

		// If requested by HTMX (i.e. by an ajax request), return only the results partial without layout
		if c.Get("Hx-Request") != "" {
			return c.Render("partials/results", data, "")
		}

		return c.Render("validate", data)
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
